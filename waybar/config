{
    "layer": "top", // Waybar at top layer
    "position": "top", // Waybar position (top|bottom|left|right)
    "margin": "5 5 1 5",
    // Choose the order of the modules
    "modules-left": [
        "custom/appmenu",
        "hyprland/workspaces",
        "wlr/taskbar"
    ],
    "modules-center": [
        "clock"
    ],
    "modules-right": [
        "pulseaudio",
        "tray",
        "cpu",
        "backlight",
        "battery",
        "custom/power"
    ],
    //***************************
    //*  Modules configuration  *
    //***************************
    "hyprland/workspaces": {
        "disable-scroll": true,
    },
    // Rofi Application Launcher
    "custom/appmenu": {
        "format": "Menu",
        "on-click": "sleep 0.2;wofi --show drun",
        "tooltip": false
    },
    "backlight": {
        "format": "{percent}% {icon}",
        "format-icons": [
            "",
            ""
        ],
        // "on-click": "brightnessctl s {percentage}%",
        "tooltip": false
    },
    "clock": {
        "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
        "format": "{:%a, %d %b, %I:%M %p}"
    },
    "pulseaudio": {
        "reverse-scrolling": 1,
        "format": "{volume}% {icon} {format_source}",
        "format-bluetooth": "{volume}% {icon} {format_source}",
        "format-bluetooth-muted": " {icon} {format_source}",
        "format-muted": " {format_source}",
        "format-source": "{volume}% ",
        "format-source-muted": "",
        "format-icons": {
            "headphone": "",
            "hands-free": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": [
                "",
                "",
                ""
            ]
        },
        "on-click": "pavucontrol",
        "min-length": 13,
    },
    // Network
    "network": {
        "format": "{ifname}",
        "format-wifi": "   {signalStrength}%",
        "format-ethernet": "  {ifname}",
        "format-disconnected": "Disconnected",
        "tooltip-format": " {ifname} via {gwaddri}",
        "tooltip-format-wifi": "  {ifname} @ {essid}\nIP: {ipaddr}\nStrength: {signalStrength}%\nFreq: {frequency}MHz\nUp: {bandwidthUpBits} Down: {bandwidthDownBits}",
        "tooltip-format-ethernet": " {ifname}\nIP: {ipaddr}\n up: {bandwidthUpBits} down: {bandwidthDownBits}",
        "tooltip-format-disconnected": "Disconnected",
        "max-length": 50,
        "on-click": "nm-connection-editor"
    },
    "wlr/taskbar": {
        "format": "{icon}",
        "tooltip-format": "{title}",
        "on-click": "activate"
    },
    "cpu": {
        "interval": 2,
        "format": "{usage}% ",
        "min-length": 6
    },
    "battery": {
        "states": {
            "warning": 30,
            "critical": 15
        },
        "format": "{capacity}% {icon}",
        "format-charging": "{capacity}% ",
        "format-plugged": "{capacity}% ",
        "format-alt": "{time} {icon}",
        "format-icons": [
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        ],
        "on-update": "$HOME/.config/waybar/scripts/check_battery.sh"
    },
    "tray": {
        "icon-size": 16,
        "spacing": 0
    },
    "custom/power": {
        "format": " ⏻ ",
        "tooltip": false,
        "on-click": "wlogout --protocol layer-shell"
    }
}